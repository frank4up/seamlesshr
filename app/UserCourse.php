<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserCourse extends Model
{
    protected $fillable = ['user_id', 'is_active','course_id'];

    public function Course()
    {
        return $this->belongsTo(Course::class);
    }
}
