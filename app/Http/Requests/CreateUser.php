<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateUser extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
	{
		switch ($this->method()) {
			case 'GET':
			case 'DELETE': {
				return [
                ];
			}
			case 'POST': {
				return [
                    'email' => "required|email|unique:users,email",
                    'password' => 'required|min:5',
                    'name' => 'required|min:3',
				];
			}
			case 'PUT':
			case 'PATCH': {
				return [
               
				];
			}
			default:
				break;
		}
    }
    
    public function messages()
	{
		return [
		];
	}
}
