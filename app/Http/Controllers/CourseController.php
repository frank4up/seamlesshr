<?php

namespace App\Http\Controllers;

use App\Course;
use App\Exports\CourseExport;
use App\Http\Requests\RegisterCourse;
use App\Jobs\CreateCourseJob;
use App\UserCourse;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

class CourseController extends Controller
{
    public function createCourse() {

        CreateCourseJob::dispatch();


        return response()->json([
            'message' => "Course creation in progress",
        ]);
    }

    public function getMyCourses() {

        $myCourses = UserCourse::whereUserId(auth()->user()->id)->with(['Course'])->get();

        return response()->json([
            'message' => "User's courses",
            'data' => $myCourses
        ]);

    }

    public function registerCourses(RegisterCourse $request) {
        
        UserCourse::create(['user_id' => auth()->user()->id ,'course_id' => $request->course_id]);

        return response()->json([
            'message' => "Course registration successfull",
        ]);

    }

    public function exportAllCourse() {
        
        return Excel::download(new CourseExport, 'courses.xlsx');
    }
}
