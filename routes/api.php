<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });






Route::group(['middleware' => 'api', 'prefix' => 'auth'], function ($router) {

    Route::post('register', 'AuthController@register');
    Route::post('login', 'AuthController@login');

    Route::get('export_courses', 'CourseController@exportAllCourse');


    Route::group(['middleware' =>  ['auth:api']], function(){


        Route::post('create_courses', 'CourseController@createCourse');
        Route::post('register_courses', 'CourseController@registerCourses');
        Route::get('get_my_courses', 'CourseController@getMyCourses');


    });

});