<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Course;
use Faker\Generator as Faker;

$factory->define(Course::class, function (Faker $faker) {
    return [
        'title' => $faker->word(),
        'description' => $faker->text(),
        'period' => $faker->randomDigit,
        'is_active' => $faker->numberBetween(0,1), // password
    ];
});
